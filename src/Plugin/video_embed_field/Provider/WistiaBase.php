<?php

namespace Drupal\wistia\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;

/**
 * Base class for Wistia Video Embed Field plugins.
 */
abstract class WistiaBase extends ProviderPluginBase {

  /**
   * Returns player settings from the system wide configuration.
   *
   * @return array
   *   Array containing player settings as a key.
   */
  protected function getGlobalPlayerSettings() {
    $globalConfig = \Drupal::configFactory()->get('wistia.settings');
    return [
      'player_settings' => [
        'video_foam' => 'true',
        'color' => $globalConfig->get('player_color'),
        'display_play_button' => $globalConfig->get('display_play_button') === 1 ? 'true' : 'false',
        'video_width' => $globalConfig->get('video_width'),
        'video_height' => $globalConfig->get('video_height'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function renderEmbedCode($width, $height, $autoplay);

  /**
   * {@inheritdoc}
   */
  abstract public function getRemoteThumbnailUrl();

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    // stub.
  }

}
