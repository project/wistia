<?php

namespace Drupal\wistia\Plugin\video_embed_field\Provider;

/**
 * Wistia playlist video provider.
 *
 * @VideoEmbedProvider(
 *   id = "wistia_playlist",
 *   title = @Translation("Wistia Playlist")
 * )
 */
class WistiaPlaylist extends WistiaBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $settings = [];
    $settings += $this->getGlobalPlayerSettings();
    // Settings can be used in template or in behaviors.
    return [
      '#theme' => 'wistia_playlist',
      '#playlist_id' => $this->getVideoId(),
      '#player_settings' => $settings,
      '#attached' => [
        'library' => [
          'wistia/wistia_playlist',
        ],
        'drupalSettings' => $settings,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    // Delegate to getLocalThumbnailUri().
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocalThumbnailUri() {
    // Get default thumbnail as playlist do not have a poster.
    $destination = \Drupal::config('media.settings')->get('icon_base_uri');
    return $destination . '/wistia.png';
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    // Example url: https://my_account.wistia.com/projects/id
    preg_match('/^https?:\/\/(.+)?(wistia.com|wi.st)\/(projects)\/(?<id>[0-9A-Za-z]+)$/', $input, $matches);
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }

}
