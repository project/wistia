<?php

namespace Drupal\wistia\Plugin\video_embed_field\Provider;

/**
 * Wistia video provider.
 *
 * @VideoEmbedProvider(
 *   id = "wistia_video",
 *   title = @Translation("Wistia Video")
 * )
 */
class WistiaVideo extends WistiaBase {

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    $settings = [];
    $settings += $this->getGlobalPlayerSettings();
    // Settings can be used in template or in behaviors.
    return [
      '#theme' => 'wistia_video',
      '#video_id' => $this->getVideoId(),
      '#player_settings' => $settings,
      '#attached' => [
        'library' => [
          'wistia/wistia_video',
        ],
        'drupalSettings' => $settings,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    // @see https://www.drupal.org/project/video_embed_field/issues/2826309
    $video_data = json_decode(file_get_contents('https://fast.wistia.net/oembed?url=' . $this->getInput() . '?embedType=async'));
    return $video_data->thumbnail_url;
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    // Example url: https://my_account.wistia.com/medias/id
    preg_match('/^https?:\/\/(.+)?(wistia.com|wi.st)\/(medias|embed)\/(?<id>[0-9A-Za-z]+)$/', $input, $matches);
    return isset($matches['id']) ? $matches['id'] : FALSE;
  }

}
