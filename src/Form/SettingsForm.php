<?php

namespace Drupal\wistia\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'wistia.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wistia_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('wistia.settings');
    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token'),
      '#description' => $this->t('Get your token at <em>https://youraccount.wistia.com/account/api</em>. It is not required for read operations.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('token'),
    ];

    $form['appearance'] = [
      '#type' => 'details',
      '#title' => $this->t('Appearance'),
      '#open' => TRUE,
    ];
    $form['appearance']['player_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Player color'),
      '#description' => $this->t('Color used for the video and playlist player.'),
      '#default_value' => $config->get('player_color'),
      '#required' => TRUE,
    ];
    $form['appearance']['video_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Video width'),
      '#default_value' => $config->get('video_width'),
      '#required' => TRUE,
    ];
    $form['appearance']['video_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Video height'),
      '#default_value' => $config->get('video_height'),
      '#required' => TRUE,
    ];
    $form['appearance']['display_play_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display play button'),
      '#description' => $this->t('Uncheck to use a custom play button.'),
      '#default_value' => $config->get('display_play_button'),
    ];

    // @todo add (optional) default player button image.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('wistia.settings')
      ->set('token', $form_state->getValue('token'))
      ->set('player_color', $form_state->getValue('player_color'))
      ->set('video_width', $form_state->getValue('video_width'))
      ->set('video_height', $form_state->getValue('video_height'))
      ->set('display_play_button', $form_state->getValue('display_play_button'))
      ->save();
  }

}
