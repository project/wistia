/**
 * @file
 * Attaches behaviors for Wistia playlist.
 */

(function($, Drupal) {
  'use strict';

  Drupal.behaviors.wistiaPlaylist = {
    attach: function (context, settings) {
      $(context).find('.wistia_playlist').once('wistiaPlaylist').each(function () {
        var $playlist = $(this);
        Wistia.playlist($playlist.data('wistia-playlist-id'), {
          version: 'v1',
          theme: 'tab',
          autoAdvance: false,
          videoOptions: {
            playButton: settings.player_settings.display_play_button,
            smallPlayButton: false,
            volumeControl: true,
            playerColor: settings.player_settings.color,
            autoPlay: false,
            videoWidth: settings.player_settings.video_width,
            videoHeight: settings.player_settings.video_height,
            inlineOptionsOnly: true,
            version: 'v1'
          },
          videoFoam: 'true',
          media_0_0: {
            autoPlay: false,
            controlsVisibleOnLoad: false
          }
        });
      });
    }
  };

})(jQuery, Drupal);
