# Wistia

Media provider and field widget for Wistia video and playlist.

Customisable:
- Player settings: playlist, button, chrome, ...
- Twig templates

Features are close to what _Video Embed Wistia_ proposes, but with the
following differences:
- Setup of default Media providers for Video and Playlist.
- Uses the Wistia API instead of the iframe, for more flexibility.

## Configuration

### Media

@todo

### Field

@todo

## Overrides

Use libraries override
https://www.drupal.org/docs/8/modules/decoupled-blocks-vuejs/override-javascript-libraries

## Dependencies

- _Media_ (core)
- _Media Library_ (core, experimental)
- _Video Embed Field_ and _Video Embed Media_ submodule

## Related modules

- [Video Embed Wistia](https://www.drupal.org/project/video_embed_wistia):
based on [Video Embed Field](https://www.drupal.org/project/video_embed_field)
so it is field based and also uses a Media provider integration.
Uses iframe embed.
- [Wistia Integration](https://www.drupal.org/project/wistia_integration):
field and CKEditor 4 based, with upload capabilities.
- [Video Wistia](https://www.drupal.org/project/video_wistia):
based on the [Video](https://www.drupal.org/project/video) module.

## Roadmap

- CKEditor 5 sections support
- Block support
